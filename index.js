'use strict';

const config = require('./config.json');

const  { createDb, createTable, insertOrUpdate, findEmptyProxyInDbById } = require("./modules/database");
const puppeteer = require('puppeteer');
const request = require('request-promise-native');
const {passRecaptcha} = require('./modules/captcha');

let browser;
let db;


const initBrowserAndTable = async () => {
    console.log("Initialization started.");
    [browser, db] = await Promise.all([puppeteer.launch(config.chrome), createDb()]);
    await createTable(db);
    console.log("Initialization finished.");
};


class Parser {
    constructor(browser, link, type) {
        this.browser = browser;
        this.link = link;
        this.type = type;
    }

    get Type() {
        return this.type;
    }

    async clean() {
        if (this.page) {
            await this.page.close();
        }
    }
}

class FreeProxyCz extends Parser {
    constructor(browser, link) {
        super(browser, link, FreeProxyCz.Type);
    }

    static get Type()  {
        return 'free-proxy.cz';
    }

    async getProxies(callBack) {
        const page = await this.browser.newPage();
        this.page = page;
        await page.goto(this.link);

        let proxies = [];

        let success = await passRecaptcha(page);

        if (!success) {
            console.log("Could not bypass the captcha.");
            return proxies;
        }

        const lastPage = await page.evaluate(() => {
            const pags = document.getElementsByClassName("paginator");
            if (pags.length === 0) {
                return -1;
            }
            const aArr = pags[0].getElementsByTagName('a');
            if (aArr.length === 0) {
                return -1;
            }
            const lastPageEl = aArr[(aArr.length - 2)];

            const result = Number(lastPageEl.innerText);
            if (Number.isInteger(result)) {
                return result;
            }
            return 1; // For sites the are only one page
        });

        const link = this.link   + "$1"; //proxylist/main

        // Without Recaptcha only five pages from beginning
        for (let index = 1; index <= lastPage; ++index) {
            // Waiting for 2 seconds to not overload the website.
            await page.waitFor(2000);
            await page.goto(link.replace("$1", index));

            success = await passRecaptcha(page);

            if (!success) {
                console.log("Could not bypass the captcha.");
                break;
            }

            const result = await page.evaluate(() => {
                const arr = [];
                const tableEl = document.getElementById("proxy_list");
                if (!tableEl) {
                    return arr;
                }
                const rows = tableEl.rows;
                for (let rowIndex = 1; rowIndex < rows.length; ++rowIndex) {
                    const row = rows[rowIndex];
                    const cells = row.cells;
                    if (cells.length !== 11) continue;
                    arr.push({
                        "ip": cells[0].innerText,
                        "port": cells[1].innerText,
                        "type": cells[2].innerText.toLowerCase()
                    });
                }
                return arr;
            });

            proxies = proxies.concat(result);
            if (callBack) {
                await callBack(result);
            }
        }
        return proxies;
    }
}

class FreeProxyList extends Parser {
    constructor(browser, link) {
        super(browser, link, FreeProxyList.Type);
    }

    static get Type()  {
        return 'free-proxy-list';
    }

    async getProxies(callBack) {
        const page = await this.browser.newPage();
        this.page = page;
        await page.goto(this.link);
        let proxies = [];
        let result;
        do {
            result = await page.evaluate(() => {
                const arr = [];
                const nextButton = document.getElementById("proxylisttable_next");
                if (!nextButton) {
                    return;
                }
                const tableEl = document.getElementById("proxylisttable");
                if (!tableEl) {
                    return;
                }
                const rows = tableEl.rows;
                const re = /yes/i;
                for (let rowIndex = 1; rowIndex < (rows.length - 1); ++rowIndex) {
                    const row = rows[rowIndex];
                    const cells = row.cells;
                    arr.push({
                        "ip": cells[0].innerText,
                        "port": cells[1].innerText,
                        "type": re.test(cells[6].innerText) ? "https" : "http"
                    })
                }
                const noData = nextButton.classList.contains("disabled");
                return {
                    "arr": arr,
                    "noData": noData
                };
            });
            if (!result) {
                break;
            }
            if (!result.noData) {
                await page.click("#proxylisttable_next > a");
            }
            proxies = proxies.concat(result.arr);
            if (callBack) {
                await callBack(result.arr);
            }
        } while (!result.noData);
        return proxies;
    }
}

const getApiKey = () => {
    if (config.data_mining["api-key"].length > 0) {
        return config.data_mining["api-key"][0];
    }
};

const removeApiKey = (api_key) => {
    const index = config.data_mining["api-key"].indexOf(api_key);
    if (index > -1) {
        console.log("Removing api key", api_key);
        config.data_mining["api-key"].splice(index, 1);
    }
};

const createLink = (proxyObject, api_key) => {
    if (!api_key) {
        return;
    }
    return config.data_mining.link.replace("$2", api_key).replace("$1", proxyObject.ip);
};

const getIPData = async (proxyObject) => {
    do {
        let api_key;
        try {
            api_key = getApiKey();
            const link = createLink(proxyObject, api_key);
            if (!link) {
                return Promise.resolve(console.log('No api link'));
            }
            const options = {
                uri: link,
                json: true
            };
            return await request(options);

        } catch (e) {
            if (e.statusCode === 403) {
                removeApiKey(api_key);
            } else {
                return Promise.reject(e);
            }
        }
    } while (true);
};



//const batchProcessIPLookup = async (proxiesArray) => {
//    ()}

const runScraper = async () => {
    console.log("Run: started");
    for (const proxyObj of config.proxies) {
        let proxy;
        console.log("Processing ", proxyObj.type);
        switch (proxyObj.type) {
            case FreeProxyList.Type:
                proxy = new FreeProxyList(browser, proxyObj.link);
                break;
            case FreeProxyCz.Type:
                proxy = new FreeProxyCz(browser, proxyObj.link);
                break;
            default:
                console.error("Unknown proxy type", proxyObj.type);
                break;
        }
        if (proxy) {
            console.log("Parsing proxy data page.");
            const source = proxy.Type;
            let count = 0;
            await proxy.getProxies(async (proxiesArray) => {
                console.log('Found on page', proxiesArray.length, 'proxies');

                const filteredArr = [];
                for (let proxyObject of proxiesArray) {
                    const found = await findEmptyProxyInDbById(proxyObject, db);
                    if (!found) {
                        filteredArr.push(proxyObject);
                    }
                }

                console.log('After filtration found', filteredArr.length, 'proxies');
                proxiesArray = filteredArr;

                console.log('Requesting data.');

                const jsonData = await Promise.all(proxiesArray.map(getIPData));
                console.log('Data received.');
                // Perform database operations sequentially.
                // See below
                for (let index = 0; index < jsonData.length; ++index) {
                    const dataObject = jsonData[index];
                    const proxyObject = proxiesArray[index];
                    await insertOrUpdate(proxyObject, dataObject, source, db);
                }
                count += jsonData.length;
                // It create too much io request to drive, then db operations started
                // const promisesPart = proxiesArray.map(processProxy(source));
            });
            console.log("On site found", count, "proxies");
            await proxy.clean();
            /*
            No need in slow down, api limit 1500 per day
            for (const pr of proxies) {
                await processProxy(proxy.Type)(pr);
            }*/
        }
    }
    console.log("Run: finished");
};

const closingResources = async () => {
    if (browser) {
        console.log('Closing browser');
        await browser.close();
    }
    if (db) {
        console.log('Closing db');
        db.close();
    }
};

const errors = async (err) => {
    console.error('Script crushed.');
    await closingResources();
    throw err;
};

initBrowserAndTable().then(runScraper).then(closingResources).
catch(errors);