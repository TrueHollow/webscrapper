
const insertSQLCommand = `insert into proxycheck (
    "IdProxy",
    "IdScrapping",
    "Ip",
    working)
values (
    $IdProxy,
    $IdScrapping,
    $Ip,
    $working)
;`;

const insertProxyCheckInDb = (proxyCheckObject, db) => {
    return new Promise((resolve, reject) => {
        db.run(insertSQLCommand, {
            $IdProxy: proxyCheckObject.IdProxy,
            $IdScrapping: proxyCheckObject.IdScrapping,
            $Ip: proxyCheckObject.Ip,
            $working: working
        }, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
};

module.exports = {
    insertProxyCheckInDb: insertProxyCheckInDb
};