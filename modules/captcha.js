'use strict';

const anticaptcha = require('./anticaptcha');
const anti_captcha_api_key = require("../captcha.json")["anti-captcha"];

// Attemp to pass captcha
const attempCount = 10;
const passRecaptcha = async (page) => {
    for (let i = 0; i < attempCount; ++i) {
        const captchaFound = await page.evaluate(() => {
            const elements = document.getElementsByClassName("g-recaptcha");
            return elements.length > 0;
        });
        if (!captchaFound) {
            return true;
        }
        console.log("Attempt number ", (i + 1), " proxy bypass.");
        await byPassCaptcha(page);
    }
    return false;
};


const byPassCaptcha = async (page) => {
    console.log('Anti-captcha: started.');
    const captchaBypass = new CaptchaBypass(anti_captcha_api_key);
    const url = await page.url();

    const websiteKey = await page.evaluate(() => {
        const elements = document.getElementsByClassName("g-recaptcha");
        if (elements.length === 0) {
            return;
        }
        const el = elements[0];
        return el.getAttribute("data-sitekey");

    });
    if (!websiteKey) {
        return;
    }

    let gRecaptchaResponse;
    try {
        const balance = await captchaBypass.getBalance();
        if (balance <= 0) {
            return console.error('Balance is too low', balance);
        }
        console.log("Anti-captcha: creating task");
        const taskId = await captchaBypass.createTaskProxyless(url, websiteKey);

        console.log("Anti-captcha: getting solution with taskId", taskId);
        const start = new Date().getTime();
        gRecaptchaResponse = await captchaBypass.getTaskSolution(taskId);
        const stop = new Date().getTime();
        console.log("Anti-captcha: solution found", (stop - start) / 1000, "seconds passed.");
    } catch (e) {
        return console.log('Anti-Captcha error:', e);
    }

    console.log(`Anti-captcha: setting gRecaptchaResponse ("${gRecaptchaResponse}") and clicking submit button.`);
    let error = await page.evaluate(gRecaptchaResponse => {
        const captchaResponse = document.getElementById("g-recaptcha-response");
        if (!captchaResponse) {
            return 'g-recaptcha-response not found';
        }
        captchaResponse.innerHTML = gRecaptchaResponse;
        const forms = document.getElementsByTagName('form');
        if (forms.length !== 1) {
            return 'wrong forms count on page ' + forms.length;
        }
        const form = forms[0];
        form.submit();
    }, gRecaptchaResponse);

    if (error) {
        return console.error("Anti-captcha:", error);
    }

    console.log(`Anti-captcha: waiting for result...`);
    await page.waitForNavigation();

    if (url !== await page.url()) {
        console.log("Anti-captcha: Invalid captcha entered - return");
        await page.goto(url);
    }
    console.log('Anti-captcha: finished.');
};



class CaptchaBypass {
    constructor(api_key) {
        this.anticaptcha = anticaptcha(api_key);
    }

    async getBalance() {
        const anticaptcha = this.anticaptcha;
        return new Promise((resolve, reject) => {
            anticaptcha.getBalance((err, balance) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(balance)
                }
            });
        });
    }

    async createTaskProxyless(url, key) {
        const anticaptcha = this.anticaptcha;
        anticaptcha.setWebsiteURL(url);
        anticaptcha.setWebsiteKey(key);

        // Is it necessary?
        anticaptcha.setUserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116");
        return new Promise((resolve, reject) => {
            anticaptcha.createTaskProxyless((err, taskId) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(taskId);
                }
            });
        });
    }

    async getTaskSolution(taskId) {
        const anticaptcha = this.anticaptcha;
        return new Promise((resolve, reject) => {
            anticaptcha.getTaskSolution(taskId, (err, taskSolution) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(taskSolution);
                }
            });
        });
    }
}

module.exports.CaptchaBypass = CaptchaBypass;
module.exports.byPassCaptcha = byPassCaptcha;
module.exports.passRecaptcha = passRecaptcha;