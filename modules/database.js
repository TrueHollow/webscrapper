'use strict';

const sqlite3 = require('sqlite3');
const config = require('../config.json');

const getUtcTimestamp = () => {
    const now = new Date();
    return now.getTime() + (now.getTimezoneOffset() * 60 * 1000);
};

const createDb = () => {
    return new Promise((resolve, reject) => {
        let dbObject = new sqlite3.cached.Database(config.sqlite.dbpath, sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE, (err) => {
            if (err) {
                reject(err);
            } else {
                console.log("Database connection ready");
                resolve(dbObject);
            }
        });
    });
};

const createIPLookUpTableSQL = `CREATE TABLE if not exists "site_scrapping"
                                (
                                  "IndexId"              INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
                                  "IdProxy"              TEXT    NOT NULL UNIQUE,
                                  "Ip"                   TEXT    NOT NULL,
                                  "port"                 NUMERIC,
                                  "type"                 TEXT,
                                  "country_name"         TEXT,
                                  "continent_name"       TEXT,
                                  "organisation"         TEXT,
                                  "is_tor"               NUMERIC,
                                  "is_proxy"             NUMERIC,
                                  "is_anonymous"         NUMERIC,
                                  "is_known_attacker"    NUMERIC,
                                  "is_known_abuser"      NUMERIC,
                                  "is_threat"            NUMERIC,
                                  "is_bogon"             NUMERIC,
                                  "status"               NUMERIC,
                                  "error"                NUMERIC,
                                  "created_at"           INTEGER NOT NULL,
                                  "last_used"            INTEGER NOT NULL,
                                  "last_check"           INTEGER NOT NULL,
                                  "lasterrormessage"     TEXT,
                                  "sourcewebsiteofproxy" TEXT
                                );  `;

const createProxycheckTableSQL = `CREATE TABLE if not exists "proxycheck" ("IndexId" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, "IdScrapping" INTEGER NOT NULL, "IdProxy" TEXT, "Ip" TEXT, "working" NUMERIC, "last_check" NUMERIC );`;

const createIPLookUpIndexSQL = 'CREATE INDEX if not exists "IdProxy_Index" ON "site_scrapping" ("IdProxy");CREATE INDEX if not exists "IndexId" ON "site_scrapping" ("IndexId");';

const createTable = (db) => {
    return new Promise((resolve, reject) => {
        db.exec(createIPLookUpTableSQL + createIPLookUpIndexSQL + createProxycheckTableSQL, (err) => {
            if (err) {
                reject(err);
            } else {
                console.log("Table ready");
                resolve();
            }
        });
    });
};

const insertCommandSQL = `insert into site_scrapping (
    "IdProxy",
    "Ip",
    "port",
    "type",
    continent_name,
    country_name,
    created_at,
    error,
    is_anonymous,
    is_bogon,
    is_known_abuser,
    is_known_attacker,
    is_proxy,
    is_threat,
    is_tor,
    last_check,
    last_used,
    lasterrormessage,
    organisation,
    sourcewebsiteofproxy,
    status)
values (
    $IdProxy,
    $Ip,
    $port,
    $type,
    $continent_name,
    $country_name,
    $created_at,
    $error,
    $is_anonymous,
    $is_bogon,
    $is_known_abuser,
    $is_known_attacker,
    $is_proxy,
    $is_threat,
    $is_tor,
    $last_check,
    $last_used,
    $lasterrormessage,
    $organisation,
    $sourcewebsiteofproxy,
    $status)
;`;

const createEmptyProxyModel = (lasterrormessage) => {
    return {
        "ip": "",
        "is_eu": false,
        "city": "",
        "region": "",
        "region_code": "",
        "country_name": "",
        "country_code": "",
        "continent_name": "",
        "continent_code": "",
        "latitude": 0,
        "longitude": 0,
        "asn": "",
        "organisation": "",
        "postal": "",
        "calling_code": "",
        "flag": "",
        "emoji_flag": "",
        "emoji_unicode": "",
        "languages": [
            {
                "name": "",
                "native": ""
            }
        ],
        "currency": {
            "name": "",
            "code": "",
            "symbol": "",
            "native": "",
            "plural": ""
        },
        "time_zone": {
            "name": "",
            "abbr": "",
            "offset": "",
            "is_dst": false,
            "current_time": ""
        },
        "threat": {
            "is_tor": false,
            "is_proxy": false,
            "is_anonymous": false,
            "is_known_attacker": false,
            "is_known_abuser": false,
            "is_threat": false,
            "is_bogon": false
        },
        "count": "0",
        "lasterrormessage": lasterrormessage
    };
};

const insertProxyInDb = (proxyObject, dataObj, source, db) => {
    const IdProxy = generatetIdProxy(proxyObject);
    if (!dataObj) {
        dataObj = createEmptyProxyModel("Error reached API Limit or could not fetch data");
    }
    return new Promise((resolve, reject) => {
        const utcTimestamp = getUtcTimestamp();
        db.run(insertCommandSQL,
            {
                $IdProxy: IdProxy,
                $Ip: proxyObject.ip,
                $port: proxyObject.port,
                $type: proxyObject.type,
                $country_name: dataObj.country_name,
                $continent_name: dataObj.continent_name,
                $created_at: utcTimestamp,
                $error: !!(dataObj.lasterrormessage),
                $is_anonymous: dataObj.threat.is_anonymous,
                $is_bogon: dataObj.threat.is_bogon,
                $is_known_abuser: dataObj.threat.is_known_abuser,
                $is_known_attacker: dataObj.threat.is_known_attacker,
                $is_proxy: dataObj.threat.is_proxy,
                $is_threat: dataObj.threat.is_threat,
                $is_tor: dataObj.threat.is_tor,
                $last_check: utcTimestamp,
                $last_used: 0,
                $lasterrormessage: dataObj.lasterrormessage,
                $organisation: dataObj.organisation,
                $sourcewebsiteofproxy: source,
                $status: true
            }, (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
    });
};

const updateCommand = `UPDATE site_scrapping 
SET
  continent_name = $continent_name,
  country_name = $country_name,
  error = $error,
  is_anonymous = $is_anonymous,
  is_bogon = $is_bogon,
  is_known_abuser = $is_known_abuser,
  is_known_attacker = $is_known_attacker,
  is_proxy = $is_proxy,
  is_threat = $is_threat,
  is_tor = $is_tor,
  last_check = $last_check,
  last_used = $last_used,
  lasterrormessage = $lasterrormessage,
  organisation = $organisation,
  status = $status
WHERE IndexId = $IndexId;`;

const updateProxyInDb = (IndexId, db, dataObj) => {
    if (!dataObj) {
        dataObj = createEmptyProxyModel("Error reached API Limit or could not fetch data");
    }
    return new Promise(((resolve, reject) => {
        const utcTimestamp = getUtcTimestamp();
        db.run(updateCommand, {
            $country_name: dataObj.country_name,
            $continent_name: dataObj.continent_name,
            $error: !!(dataObj.lasterrormessage),
            $is_anonymous: dataObj.threat.is_anonymous,
            $is_bogon: dataObj.threat.is_bogon,
            $is_known_abuser: dataObj.threat.is_known_abuser,
            $is_known_attacker: dataObj.threat.is_known_attacker,
            $is_proxy: dataObj.threat.is_proxy,
            $is_threat: dataObj.threat.is_threat,
            $is_tor: dataObj.threat.is_tor,
            $last_check: utcTimestamp,
            $last_used: utcTimestamp,
            $lasterrormessage: dataObj.lasterrormessage,
            $organisation: dataObj.organisation,
            $status: true,
            $IndexId: IndexId
        }, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    }));
};

const findEmptyProxyInDbById = (proxyObject, db) => {
    const IdProxy = generatetIdProxy(proxyObject);
    return new Promise((resolve, reject) => {
        db.get('SELECT IndexId FROM site_scrapping WHERE IdProxy == ? AND country_name != "";', [IdProxy], (err, row) => {
            if (err) {
                return reject(err);
            }
            resolve(row);
        })
    });
};

const findProxyInDbById = (proxyObject, db) => {
    const IdProxy = generatetIdProxy(proxyObject);
    return new Promise((resolve, reject) => {
        db.get("SELECT IndexId FROM site_scrapping WHERE IdProxy == ?", [IdProxy], (err, row) => {
            if (err) {
                return reject(err);
            }
            resolve(row);
        })
    });
};

const findProxyInDbBySite = (sourceSite, db) => {
    return new Promise((resolve, reject) => {
        db.all("SELECT IdProxy FROM site_scrapping WHERE sourcewebsiteofproxy == ?", [sourceSite], (err, row) => {
            if (err) {
                return reject(err);
            }
            resolve(row);
        })
    });
};
const findProxyInDbByCountry = (sourceSite, db) => {
    return new Promise((resolve, reject) => {
        db.all("SELECT IdProxy FROM site_scrapping WHERE country_name == ?", [sourceSite], (err, row) => {
            if (err) {
                return reject(err);
            }
            resolve(row);
        })
    });
};


const generatetIdProxy = (proxyObject) => {
    return proxyObject.type + "://" + proxyObject.ip + ":" + proxyObject.port;
};

const insertOrUpdate = async (proxyObject, dataObject, source, db) => {
    // Add Port and Type seperate in the database like:
    // IdProxy: 'socks5://40.87.67.196:3128'
    // ip: 40.87.67.196
    // port: 3128
    // type: socks5

    const rowId = await findProxyInDbById(proxyObject, db);
    if (rowId) {
        await updateProxyInDb(rowId.IndexId,db, dataObject);
    } else {
        await insertProxyInDb(proxyObject, dataObject, source,db);
    }
};


module.exports = {
    createDb: createDb,
    createIPLookUpTableSQL: createIPLookUpTableSQL,
    createIPLookUpIndexSQL: createIPLookUpIndexSQL,
    createTable: createTable,
    insertCommandSQL: insertCommandSQL,
    insertProxyInDb: insertProxyInDb,
    updateProxyInDb: updateProxyInDb,
    insertOrUpdate: insertOrUpdate,
    findProxyInDbBySite: findProxyInDbBySite,
    findProxyInDbByCountry: findProxyInDbByCountry,
    findEmptyProxyInDbById: findEmptyProxyInDbById
};