const  { createDb, createTable, findProxyInDbByCountry, findProxyInDbBySite } = require("./modules/database");
const axios = require('axios');
const config = require('./config.json');
const httpsProxyAgent = require('https-proxy-agent');
const httpProxyAgent = require('http-proxy-agent');
const socksProxyAgent = require('socks-proxy-agent');

// STRUCTURE
// var service is the url to test the proxy on.
// if  the proxy can send a get-request to that url it is valid.
// TODO: save result in DATABASE.
const service = config['whoisip_services'][0]
let db;


const initTable = async () => {
    console.log("Initialization started.");
    db = await createDb();
    await createTable(db);}



const requested =  new Set()
const working =  new Set()


const doRequestWithProxy = (config)=>{
    axios.request(config).then((res) => {
        let returnedIP = res.data.substring(0,res.data.length-1)
        working.add(returnedIP)
        console.log("We found the following IP:" + returnedIP)
        console.log("Source:" + res.request.agent.options.href)
        console.log("Total Working: "+ working.size , "Total Requested:" + requested.size + "\n")
    }).catch(
        err => {
            //TODO Log error into DB
            //console.log(err)
        })

}

const checkProxyByProtocol = (address,port,protocol,proxy)=>{
    // Will define the agent depending on the protocol
    // Problem here is maybe the protocol is not optimal
    // or maybe the address we use https://xyz should be http://xyz 

    requested.add(address)
    var agent
    if (protocol === 'http:') {
        // It is to note that the agent depends on the requested url
        // So right now we are requesting an https website
        agent = new httpsProxyAgent(proxy);
    }
    else if (protocol === 'https:') {
        agent = new httpsProxyAgent(proxy);
    }
    else{
        agent = new socksProxyAgent(proxy);
    }
    var config = {url: service, httpsAgent: agent}
    doRequestWithProxy(config)
}

// Example for single Proxy lookup. Here we could try different protocols
const checkHTTPSProxy = (address,port,protocol,proxy)=>{
    requested.add(address)
    var agent = new httpsProxyAgent(protocol + "//"+address+":"+port);
    var config = {url: service, httpsAgent: agent}
    doRequestWithProxy(config)
}

const checkHTTPProxy = (address,port,protocol,proxy)=>{
    requested.add(address)
    var agent = new httpProxyAgent(protocol + "//"+address+":"+port);
    var config = {url: service, httpsAgent: agent}
    doRequestWithProxy(config)
}



// Here we check in the database for proxies and the query them by requesting the address in the conifg
// It is basically an IP Lookup
const main = async () => {
    await initTable()
    console.log("Hello, we will now check the proxies we scraped for germany")
    console.log("Lets go!")
    let proxies = await findProxyInDbBySite("free-proxy.cz",db)
    //let proxies = await findProxyInDbByCountry("Germany",db)
    for (let index = 0; index < proxies.length; index++) {
        const proxy = proxies[index]['IdProxy'];
        let protocol = proxy.split("/")[0]
        let singleProxy = proxy.split("/")[2]
        let address = singleProxy.split(":")[0]
        let port = singleProxy.split(":")[1]
        // Currently working. Might be room for improvement because I'm not sure if the protocol is always right: 
        checkProxyByProtocol(address,port,protocol,proxy)
    }
}

main()



// Ignore the following notes:
//// ANOTHER IP CHECK https://ip-api.io/#demo
// https://gimmeproxy.com/api/getProxy
// https://gimmeproxy.com/api/getProxy?country=CH,DE